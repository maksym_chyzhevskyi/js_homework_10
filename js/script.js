'use strict';

function getTabContent() {
   let allTabsItems = document.querySelectorAll(".tabs-title");
   let allContents = document.querySelectorAll(".content");
   let tabContent;

   allTabsItems.forEach(element => {
      element.addEventListener("click", chooseTabsItems);
   })

   function chooseTabsItems() {
      allTabsItems.forEach(element => {
         element.classList.remove("active");
      });
      this.classList.add("active");
      tabContent = this.getAttribute("tabs-title-item");
      chooseContent(tabContent);
   }

   function chooseContent(tabContent) {
      allContents.forEach(element => {
         element.classList.contains(tabContent) ? element.classList.add("content-on") : element.classList.remove("content-on");
      });
   }
}

getTabContent()










